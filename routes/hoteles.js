/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.hotel.create({
      idDepartamento: req.body.idDepartamento,
      nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/hoteles');
  });
});

//Read
router.get('/', function(req, res) {
  models.hotel.findAll().then(function(hotel) {
    res.json(hotel);
  });
});

//Update
router.put('/', function(req, res) {
    models.hotel.update({
        nombre: req.body.nombre
    },{
        where: {
            idHotel: req.body.idHotel
        }
    }).then(function(){
        res.redirect(303, '/hoteles');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.hotel.destroy({
        where: {
            idHotel: req.body.idHotel
        }
    }).then(function() {
        res.redirect(303, '/hoteles');
    });
});

module.exports = router;