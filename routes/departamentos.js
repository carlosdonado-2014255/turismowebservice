/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.departamento.create({
    nombre: req.body.nombre
  }).then(function() {
      res.redirect(303, '/departamentos');
  });
});

//Read
router.get('/', function(req, res) {
  models.departamento.findAll().then(function(departamento) {
    res.json(departamento);
  });
});

//Update
router.put('/', function(req, res) {
    models.departamento.update({
        nombre: req.body.nombre
    },{
        where: {
            idDepartamento: req.body.idDepartamento
        }
    }).then(function(){
        res.redirect(303, '/departamentos');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.departamento.destroy({
        where: {
            idDepartamento: req.body.idDepartamento
        }
    }).then(function() {
        res.redirect(303, '/departamentos');
    });
});

module.exports = router;