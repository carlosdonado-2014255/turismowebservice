 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.habitacion.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/habitaciones');
  });
});

//Read
router.get('/', function(req, res) {
  models.habitacion.findAll().then(function(habitacion) {
    res.json(habitacion);
  });
});

//Update
router.put('/update', function(req, res) {
    models.habitacion.update({
        nombre: req.body.nombre
    },{
        where: {
            idHabitacion: req.body.idHabitacion
        }
    }).then(function(){
        res.redirect(303, '/habitaciones');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.habitacion.destroy({
        where: {
            idHabitacion: req.body.idHabitacion
        }
    }).then(function() {
        res.redirect(303, '/habitaciones');
    });
});

module.exports = router;