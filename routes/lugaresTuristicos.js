/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.lugarTuristico.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/lugaresTuristicos');
  });
});

//Read
router.get('/', function(req, res) {
  models.lugarTuristico.findAll().then(function(lugarTuristico) {
    res.json(lugarTuristico);
  });
});

//Update
router.put('/', function(req, res) {
    models.lugarTuristico.update({
        nombre: req.body.nombre
    },{
        where: {
            idLugarTuristico: req.body.idLugarTuristico
        }
    }).then(function(){
        res.redirect(303, '/lugaresTuristicos');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.lugarTuristico.destroy({
        where: {
            idLugarTuristico: req.body.idLugarTuristico
        }
    }).then(function() {
        res.redirect(303, '/lugaresTuristicos');
    });
});

module.exports = router;