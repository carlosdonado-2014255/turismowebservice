/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.factura.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/facturas');
  });
});

//Read
router.get('/', function(req, res) {
  models.factura.findAll().then(function(factura) {
    res.json(factura);
  });
});

//Update
router.put('/', function(req, res) {
    models.factura.update({
        nombre: req.body.nombre
    },{
        where: {
            idFactura: req.body.idFactura
        }
    }).then(function(){
        res.redirect(303, '/facturas');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.factura.destroy({
        where: {
            idFactura: req.body.idFactura
        }
    }).then(function() {
        res.redirect(303, '/facturas');
    });
});

module.exports = router;