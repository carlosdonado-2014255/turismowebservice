/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.rol.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/roles');
  });
});

//Read
router.get('/', function(req, res) {
  models.rol.findAll().then(function(rol) {
    res.json(rol);
  });
});

//Update
router.put('/', function(req, res) {
    models.rol.update({
        nombre: req.body.nombre
    },{
        where: {
            idRol: req.body.idRol
        }
    }).then(function(){
        res.redirect(303, '/roles');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.rol.destroy({
        where: {
            idRol: req.body.idRol
        }
    }).then(function() {
        res.redirect(303, '/roles');
    });
});

module.exports = router;