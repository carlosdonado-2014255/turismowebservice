/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.reservacion.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/reservaciones');
  });
});

//Read
router.get('/', function(req, res) {
  models.reservacion.findAll().then(function(reservacion) {
    res.json(reservacion);
  });
});

//Update
router.put('/', function(req, res) {
    models.reservacion.update({
        nombre: req.body.nombre
    },{
        where: {
            idReservacion: req.body.idReservacion
        }
    }).then(function(){
        res.redirect(303, '/reservaciones');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.reservacion.destroy({
        where: {
            idReservacion: req.body.idReservacion
        }
    }).then(function() {
        res.redirect(303, '/reservaciones');
    });
});

module.exports = router;