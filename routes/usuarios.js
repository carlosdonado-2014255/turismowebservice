/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.usuario.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/usuarios');
  });
});

//Read
router.get('/', function(req, res) {
  models.usuario.findAll().then(function(usuario) {
    res.json(usuario);
  });
});

//Update
router.put('/', function(req, res) {
    models.usuario.update({
        nombre: req.body.nombre
    },{
        where: {
            idUsuario: req.body.idUsuario
        }
    }).then(function(){
        res.redirect(303, '/usuarios');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.usuario.destroy({
        where: {
            idUsuario: req.body.idUsuario
        }
    }).then(function() {
        res.redirect(303, '/usuarios');
    });
});

module.exports = router;