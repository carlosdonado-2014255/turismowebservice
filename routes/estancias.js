/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.estancia.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/estancias');
  });
});

//Read
router.get('/', function(req, res) {
  models.estancia.findAll().then(function(estancia) {
    res.json(estancia);
  });
});

//Update
router.put('/', function(req, res) {
    models.estancia.update({
        nombre: req.body.nombre
    },{
        where: {
            idEstancia: req.body.idEstancia
        }
    }).then(function(){
        res.redirect(303, '/estancias');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.estancia.destroy({
        where: {
            idEstancia: req.body.idEstancia
        }
    }).then(function() {
        res.redirect(303, '/estancias');
    });
});

module.exports = router;