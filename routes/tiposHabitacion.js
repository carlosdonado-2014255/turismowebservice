 /* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var models  = require('../models');
var express = require('express');
var router  = express.Router();

//Create
router.post('/', function(req, res) {
  models.tipoHabitacion.create({
    nombre: req.body.nombre
  }).then(function() {
    res.redirect(303, '/tiposHabitacion');
  });
});

//Read
router.get('/', function(req, res) {
  models.tipoHabitacion.findAll().then(function(tipoHabitacion) {
    res.json(tipoHabitacion);
  });
});

//Update
router.put('/', function(req, res) {
    models.tipoHabitacion.update({
        nombre: req.body.nombre
    },{
        where: {
            idtipoHabitacion: req.body.idtipoHabitacion
        }
    }).then(function(){
        res.redirect(303, '/tiposHabitacion');
    });
});

//Delete
router.delete('/', function(req, res) {
    models.tipoHabitacion.destroy({
        where: {
            idtipoHabitacion: req.body.idtipoHabitacion
        }
    }).then(function() {
        res.redirect(303, '/tiposHabitacion');
    });
});

module.exports = router;