var express = require('express');
var redirect = require("express-redirect");
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users  = require('./routes/users');
var departamentos = require('./routes/departamentos');
var estancias = require('./routes/estancias');
var facturas = require('./routes/facturas');
var habitaciones = require('./routes/habitaciones');
var hoteles = require('./routes/hoteles');
var lugaresTuristicos = require('./routes/lugaresTuristicos');
var reservaciones = require('./routes/reservaciones');
var roles = require('./routes/roles');
var tiposHabitacion = require('./routes/tiposHabitacion');
var usuarios = require('./routes/usuarios');


var app = express();
redirect(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/departamentos', departamentos);
app.use('/estancias', estancias);
app.use('/facturas', facturas);
app.use('/habitaciones', habitaciones);
app.use('/hoteles', hoteles);
app.use('/lugaresTuristicos', lugaresTuristicos);
app.use('/reservaciones', reservaciones);
app.use('/roles', roles);
app.use('/tiposhabitacion', tiposHabitacion);
app.use('/usuarios', usuarios);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: (app.get('env') === 'development') ? err : {}
  });
});


module.exports = app;
