/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var LugarTuristico = sequelize.define("lugarTuristico", {
        idLugarTuristico: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: DataTypes.STRING},
		descripcion: {type: DataTypes.STRING},
    }, {
        classMethods: {
            associate: function(models) {
                LugarTuristico.belongsTo(models.departamento, {
                    onDelete: "CASCADE",
                    foreignKey: 'idDepartamento', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'lugarTuristico'
    });
    return LugarTuristico;
};