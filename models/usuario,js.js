/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Usuario = sequelize.define("usuario", {
        idUsuario: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: DataTypes.STRING}, 
		correo: {type: DataTypes.STRING},
		nick: {type: DataTypes.STRING},
		clave: {type: DataTypes.STRING}
    }, {
        classMethods: {
            associate: function(models) {
                Usuario.belongsTo(models.rol, {
                    onDelete: "CASCADE",
                    foreignKey: 'idRol', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'usuario'
    });
    return Usuario;
};