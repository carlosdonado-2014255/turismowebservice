/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Rol = sequelize.define("rol", {
        idRol: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: DataTypes.STRING}
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'rol'
    });
    return Rol;
};