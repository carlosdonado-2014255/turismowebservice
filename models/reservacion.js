/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Reservacion = sequelize.define("reservacion", {
        idReservacion: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        fechaEntrada: {type: DataTypes.DATE},
		deposito: {type: DataTypes.DECIMAL}		
    }, {
        classMethods: {
            associate: function(models) {
                Reservacion.belongsTo(models.usuario, {
                    onDelete: "CASCADE",
                    foreignKey: 'idUsuario', allowNull: false
                });
				Reservacion.belongsTo(models.habitacion, {
                    onDelete: "CASCADE",
                    foreignKey: 'idHabitacion', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'reservacion'
    });
    return Reservacion;
};