/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Factura = sequelize.define("factura", {
        idFactura: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: DataTypes.STRING},
        nit: {type: DataTypes.STRING},
        fechaHora: {type: DataTypes.DATE},
        total: {type: DataTypes.DECIMAL},
        direccion: {type: DataTypes.STRING}
    }, {
        classMethods: {
            associate: function(models) {
                Factura.belongsTo(models.estancia, {
                    onDelete: "CASCADE",
                    foreignKey: 'idEstancia', 
                    allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'factura'
    });
    
    return Factura;
};