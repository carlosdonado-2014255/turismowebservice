/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
"use strict";

module.exports = function(sequelize, DataTypes) {
  var TipoHabitacion = sequelize.define("tipoHabitacion", {
      idTipoHabitacion: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
      nombre: {type: DataTypes.STRING},
	  descripcion: {type: DataTypes.STRING}
  }, {
      timestamps: false,
      freezeTableName: true,
      tableName: 'tipohabitacion'
  });
  return TipoHabitacion;
};