/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Habitacion = sequelize.define("habitacion", {
        idHabitacion: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        numero: {type: DataTypes.INTEGER},
		adultos: {type: DataTypes.INTEGER},
		ninos: {type: DataTypes.INTEGER},
		costo: {type: DataTypes.DECIMAL},
		descripcion: {type: DataTypes.STRING}
    }, {
        classMethods: {
            associate: function(models) {
                Habitacion.belongsTo(models.hotel, {
                    onDelete: "CASCADE",
                    foreignKey: 'idHotel', allowNull: false
                });
				Habitacion.belongsTo(models.tipoHabitacion, {
                    onDelete: "CASCADE",
                    foreignKey: 'idTipoHabitacion', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'habitacion'
    });
    return Habitacion;
};