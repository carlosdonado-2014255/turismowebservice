/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Hotel = sequelize.define("hotel", {
        idHotel: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        nombre: {type: DataTypes.STRING} 
    }, {
        classMethods: {
            associate: function(models) {
                Hotel.belongsTo(models.departamento, {
                    onDelete: "CASCADE",
                    foreignKey: 'idDepartamento', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'hotel'
    });
    return Hotel;
};