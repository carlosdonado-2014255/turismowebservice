/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


"use strict";

module.exports = function(sequelize, DataTypes) {
    var Estancia = sequelize.define("estancia", {
        idEstancia: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        fechaEntrada: {type: DataTypes.DATE},
		fechaSalida: {type: DataTypes.DATE}		
    }, {
        classMethods: {
            associate: function(models) {
                Estancia.belongsTo(models.habitacion, {
                    onDelete: "CASCADE",
                    foreignKey: 'idHabitacion', allowNull: false
                });
				Estancia.belongsTo(models.usuario, {
                    onDelete: "CASCADE",
                    foreignKey: 'idUsuario', allowNull: false
                });
            }
        },
        timestamps: false,
        freezeTableName: true,
        tableName: 'estancia'
    });
    return Estancia;
};